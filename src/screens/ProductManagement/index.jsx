import { Box, Container, makeStyles } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { getProducts } from 'src/actions/productActions';
import { getProductTypes } from 'src/actions/productTypeActions';
import { getUsers } from 'src/actions/userActions';
import ProductListView from './ProductListView';

const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: '100%',
    paddingTop: theme.spacing(9),
    paddingBottom: theme.spacing(3),
  },
}));

function ProductManagementScreen() {
  const classes = useStyles();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProducts());
    dispatch(getUsers());
    dispatch(getProductTypes());
  }, [dispatch]);

  return (
    <Container className={classes.root} maxWidth="lg">
      <Box mt={3}>
        <ProductListView />
      </Box>
    </Container>
  );
}

export default ProductManagementScreen;
