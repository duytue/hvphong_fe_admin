import React from 'react';
import MaterialTable from 'material-table';
import { makeStyles } from '@material-ui/core/styles';
import { Avatar, Button, Chip, Grid } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { Add } from '@material-ui/icons';
import { useHistory } from 'react-router';
import { deleteProduct } from 'src/actions/productActions.js';
import { getFirstImage } from 'src/helpers/commonHelper.js';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    justifyContent: 'center',
    paddingTop: 10,
  },
  header: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  large: {
    width: theme.spacing(10),
    height: theme.spacing(10),
  },
}));

function ProductListView() {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const { products } = useSelector((state) => state.product);
  const { productTypes } = useSelector((state) => state.productType);
  const { users } = useSelector((state) => state.user);

  const getNameOfUsers = (userId) => {
    if (users) {
      const result = users.find((user) => user.id === userId);
      return result ? result.name : '';
    }
  };

  const getDescriptionOfProductType = (productTypeId) => {
    if (productTypes) {
      const result = productTypes.find(
        (productType) => productType.id === productTypeId
      );
      return result ? result.description : '';
    }
  };

  const handleAddProductClick = () => {
    history.push('/admin/product-management/add-product');
  };

  return (
    <Grid container className={classes.root} spacing={3}>
      <Grid item xs={12} md={12} className={classes.header}>
        <Button
          color="primary"
          variant="contained"
          startIcon={<Add />}
          onClick={handleAddProductClick}
        >
          Thêm sản phẩm
        </Button>
      </Grid>
      <Grid item xs={12} md={12}>
        {products && (
          <MaterialTable
            title="Quản lý sản phẩm"
            columns={[
              {
                title: 'Ảnh',
                field: 'productImages',
                render: (rowData) => {
                  return (
                    <Avatar
                      variant="square"
                      className={classes.large}
                      src={
                        rowData.productImages &&
                        getFirstImage(rowData.productImages[0].url)
                      }
                    />
                  );
                },
              },
              { title: 'Tên sản phẩm', field: 'title' },
              { title: 'Diện tích', field: 'spread' },
              { title: 'Khu vực', field: 'area' },
              { title: 'Giá', field: 'price' },
              { title: 'Mô tả', field: 'description' },
              {
                title: 'Loại sản phẩm',
                field: 'productTypeId',
                render: (rowData) =>
                  productTypes &&
                  getDescriptionOfProductType(rowData.productTypeId),
              },
              {
                title: 'Tạo bởi',
                field: 'userId',
                render: (rowData) => users && getNameOfUsers(rowData.userId),
              },
              {
                title: 'isDeleted',
                field: 'isDeleted',
                render: (rowData) =>
                  rowData.isDeleted ? (
                    <Chip label="TRUE" variant="outlined" color="secondary" />
                  ) : (
                    <Chip label="FALSE" variant="outlined" color="primary" />
                  ),
              },
            ]}
            editable={{
              onRowDelete: (oldData) => dispatch(deleteProduct(oldData.id)),
            }}
            data={products}
            options={{
              actionsColumnIndex: -1,
            }}
            actions={[
              {
                icon: 'edit',
                tooltip: 'Edit Product',
                onClick: (event, rowData) =>
                  history.push(
                    `/admin/product-management/edit-product/${rowData.id}`
                  ),
              },
            ]}
          />
        )}
      </Grid>
    </Grid>
  );
}

export default ProductListView;
