import { Box, Container, makeStyles } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router';
import { getProductTypes } from 'src/actions/productTypeActions';
import ProductForm from './ProductForm';

const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: '100%',
    paddingTop: theme.spacing(9),
    paddingBottom: theme.spacing(3),
  },
}));

function ProductFormView() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { productId } = useParams();

  useEffect(() => {
    dispatch(getProductTypes());
  }, [dispatch]);

  return (
    <Container className={classes.root} maxWidth="lg">
      <Box mt={3}>
        <ProductForm productId={productId} />
      </Box>
    </Container>
  );
}

export default ProductFormView;
