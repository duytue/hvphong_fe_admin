import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { DropzoneArea } from 'material-ui-dropzone';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import {
  Button,
  Grid,
  TextField,
  InputLabel,
  Select,
  MenuItem,
  FormControl,
  GridList,
  GridListTile,
  IconButton,
  GridListTileBar,
  Box,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import productAPI from '../../../api/productAPI';
import imagesAPI from '../../../api/imagesAPI';
import { useDispatch, useSelector } from 'react-redux';
import {
  clearProduct,
  deleteImageFromProduct,
  getProduct,
} from 'src/actions/productActions';
import { CLOUDINARY_ASSET_FOLDER } from 'src/constants/constants';

const useStyles = makeStyles((theme) => ({
  formControl: {
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
  },
  titleBar: {
    background: 'none',
  },
}));

function ProductForm({ productId }) {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const { productTypes } = useSelector((state) => state.productType);
  const { product } = useSelector((state) => state.product);

  const [files, setFiles] = useState([]);
  const [isSubmitForm, setSubmitForm] = useState(false);

  const handleChangeFile = (files) => {
    setFiles(files);
  };

  const splitImageUrl = (images) => {
    if (images) {
      return images.split(',');
    }
    return null;
  };

  const addImagesToProduct = (productId, images, mode, oldImages = null) => {
    let promises = [];

    // begin upload images to cloudinary
    images.forEach((image) => {
      const data = new FormData();
      data.append('file', image);
      data.append('upload_preset', 'wthcwne2');
      data.append('folder', CLOUDINARY_ASSET_FOLDER);

      promises.push(
        imagesAPI
          .uploadImages(data)
          .then((response) => {
            return response.data.secure_url;
          })
          .catch((error) => {
            return error;
          })
      );
    });

    Promise.all(promises).then((result) => {
      if (oldImages && oldImages.length > 0) {
        result = [...result, oldImages];
      }
      const imageUrlsString = result.join(',');

      switch (mode) {
        case 'add':
          imagesAPI.addProductImages(productId, imageUrlsString).then(() => {
            enqueueSnackbar('Product added successfully!', {
              variant: 'success',
            });
            setSubmitForm(false);
            history.push('/admin/product-management');
          });
          break;

        case 'update':
          imagesAPI.updateProductImages(productId, imageUrlsString).then(() => {
            enqueueSnackbar('Product updated successfully!', {
              variant: 'success',
            });
            setSubmitForm(false);
            history.push('/admin/product-management');
          });
          break;
        default:
          enqueueSnackbar('Unknown error!', {
            variant: 'error',
          });
      }
    });
  };

  useEffect(() => {
    if (productId) {
      dispatch(getProduct(productId));
    }
    // clear product from redux store when component un-mounted
    return () => {
      dispatch(clearProduct());
    };
  }, [dispatch, productId]);

  return (
    <div className={classes.root}>
      <Grid>
        <Grid item xs={12} md={12}>
          <h1 style={{ fontSize: '2rem' }} className={classes.paper}>
            {productId ? 'Cập nhật sản phẩm' : 'Thêm mới sản phẩm'}
          </h1>
        </Grid>

        <Grid item xs={12} md={12}>
          <Formik
            enableReinitialize
            initialValues={{
              title: product.title || '',
              spread: product.spread || '',
              area: product.area || '',
              price: product.price || '',
              description: product.description || '',
              productTypeId: product.productTypeId || '',
              files:
                (product.productImages &&
                  splitImageUrl(product.productImages[0].url)) ||
                [],
            }}
            onSubmit={async (values) => {
              const productValues = {
                title: values.title,
                spread: values.spread,
                area: values.area,
                price: values.price,
                description: values.description,
                productTypeId: parseInt(values.productTypeId),
                id: productId,
              };
              setSubmitForm(true);

              if (files.length < 1 && values.files.length < 1) {
                enqueueSnackbar('Chưa có ảnh của sản phẩm', {
                  variant: 'warning',
                });
                setSubmitForm(false);
                return;
              }

              if (productId) {
                productAPI.updateProduct(productValues).then(() => {
                  addImagesToProduct(productId, files, 'update', values.files);
                });
              } else {
                productAPI.addProduct(productValues).then((response) => {
                  const productId = response.data.data.id;
                  addImagesToProduct(productId, files, 'add');
                });
              }
            }}
          >
            {({ handleSubmit, values, handleChange }) => (
              <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                  <Grid item md={6} xs={12}>
                    <TextField
                      id="title"
                      name="title"
                      placeholder="Tên sản phẩm"
                      label="Tên Sản Phẩm"
                      fullWidth
                      onChange={handleChange}
                      value={values.title}
                    />
                  </Grid>
                  <Grid item md={6} xs={12}>
                    <TextField
                      id="spread"
                      name="spread"
                      placeholder="Diện tích"
                      label="Diện tích"
                      fullWidth
                      onChange={handleChange}
                      value={values.spread}
                    />
                  </Grid>
                  <Grid item md={6} xs={12}>
                    <TextField
                      id="area"
                      name="area"
                      placeholder="Khu vực"
                      label="Khu vực"
                      fullWidth
                      onChange={handleChange}
                      value={values.area}
                    />
                  </Grid>
                  <Grid item md={6} xs={12}>
                    <TextField
                      id="price"
                      name="price"
                      placeholder="Giá"
                      label="Giá"
                      fullWidth
                      onChange={handleChange}
                      value={values.price}
                    />
                  </Grid>
                  <Grid item md={6} xs={12}>
                    <TextField
                      id="description"
                      name="description"
                      placeholder="Mô tả chi tiết / Chú thích"
                      label="Mô tả chi tiết / Chú thích"
                      fullWidth
                      multiline
                      onChange={handleChange}
                      value={values.description}
                    />
                  </Grid>
                  <Grid item md={6} xs={12}>
                    <FormControl className={classes.formControl} fullWidth>
                      <InputLabel id="productTypeId-label">
                        Loại sản phẩm
                      </InputLabel>
                      <Select
                        labelId="productTypeId-label"
                        id="productTypeId"
                        name="productTypeId"
                        value={values.productTypeId}
                        onChange={handleChange}
                        fullWidth
                      >
                        {productTypes &&
                          productTypes.map((productType) => (
                            <MenuItem
                              key={productType.id}
                              value={productType.id}
                            >
                              {productType.description}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={12} xs={12}>
                    <Box>
                      <DropzoneArea
                        onChange={handleChangeFile}
                        acceptedFiles={['image/*']}
                        showPreviews
                        showPreviewsInDropzone={false}
                      />
                    </Box>

                    <Box paddingTop={2}>
                      <GridList className={classes.gridList} cols={2.5}>
                        {values.files.map((image) => (
                          <GridListTile key={image}>
                            <img src={image} alt={image} />
                            <GridListTileBar
                              classes={{
                                root: classes.titleBar,
                                title: classes.title,
                              }}
                              actionIcon={
                                <IconButton
                                  onClick={() =>
                                    dispatch(deleteImageFromProduct(image))
                                  }
                                >
                                  <HighlightOffIcon color="error" />
                                </IconButton>
                              }
                            />
                          </GridListTile>
                        ))}
                      </GridList>
                    </Box>
                  </Grid>
                  <Grid item xs={12} style={{ textAlign: 'center' }}>
                    <Button
                      variant="contained"
                      color="secondary"
                      style={{ marginRight: 10 }}
                      onClick={() => history.goBack()}
                    >
                      Huỷ
                    </Button>
                    <Button
                      type="submit"
                      variant="contained"
                      color="primary"
                      disabled={isSubmitForm}
                      style={{ marginLeft: 10 }}
                    >
                      {productId ? 'Cập nhật sản phẩm' : 'Thêm sản phẩm'}
                    </Button>
                  </Grid>
                </Grid>
              </form>
            )}
          </Formik>
        </Grid>
      </Grid>
    </div>
  );
}

export default ProductForm;
