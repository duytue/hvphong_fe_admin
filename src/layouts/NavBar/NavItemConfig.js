import * as Icon from 'react-feather';

export default [
  {
    title: 'Product Management',
    icon: Icon.Package,
    href: '/admin/product-management',
    isAuth: false,
  },
  {
    title: 'Product Type Management',
    icon: Icon.Tag,
    href: '/admin/product-type-management',
    isAuth: true
  },
  {
    title: 'User Management',
    icon: Icon.User,
    href: '/admin/user-management',
    isAuth: false,
  },
];
